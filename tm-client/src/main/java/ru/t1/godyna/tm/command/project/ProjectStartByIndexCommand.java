package ru.t1.godyna.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private final String NAME = "project-start-by-index";

    @NotNull
    private final String DESCRIPTION = "Start project by index.";

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectChangeStatusByIndexRequest request =
                new ProjectChangeStatusByIndexRequest(getToken(), index, Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
