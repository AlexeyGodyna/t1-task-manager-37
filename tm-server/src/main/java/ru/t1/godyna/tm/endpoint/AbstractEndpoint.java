package ru.t1.godyna.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.service.IServiceLocator;
import ru.t1.godyna.tm.dto.request.AbstractUserRequest;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.exception.user.AccessDeniedException;
import ru.t1.godyna.tm.model.Session;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        return getServiceLocator().getAuthService().validateToken(token);
    }

    @NotNull
    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        return getServiceLocator().getAuthService().validateToken(token);
    }

}
