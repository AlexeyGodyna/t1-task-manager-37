package ru.t1.godyna.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@Nullable String userId);

    @NotNull
    List<M> findAll(@Nullable String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id);

    int getSize(@NotNull String userId);

    @Nullable
    M add(@Nullable String userId, @Nullable M model);

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    M removeOne(@Nullable String userId, @Nullable M model);

    void removeAll(@Nullable String userId);

}
