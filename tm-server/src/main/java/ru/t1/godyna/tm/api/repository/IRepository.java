package ru.t1.godyna.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository <M extends AbstractModel> {

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    M add(@Nullable final M model);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    boolean existsById(@Nullable String id);

    @Nullable
    M findOneById(@Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable Integer index);

    @Nullable
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    @Nullable
    M update(@NotNull M model);

    void removeAll(@Nullable Collection<M> collection);

    void removeAll();

    long getSize();

}
