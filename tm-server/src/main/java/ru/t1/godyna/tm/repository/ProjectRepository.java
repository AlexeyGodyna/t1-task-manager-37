package ru.t1.godyna.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.constant.IDBConstant;
import ru.t1.godyna.tm.api.repository.IProjectRepository;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Getter
    @NotNull
    private final String tableName = IDBConstant.PROJECT_TABLE;

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull Project fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(IDBConstant.ROW_ID));
        project.setName(row.getString(IDBConstant.PROJECT_NAME));
        project.setDescription(row.getString(IDBConstant.PROJECT_DESCRIPTION));
        project.setUserId(row.getString(IDBConstant.USER_ID));
        project.setStatus(Status.toStatus(row.getString(IDBConstant.PROJECT_STATUS)));
        project.setCreated(row.getTimestamp(IDBConstant.PROJECT_CREATED));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@NotNull final Project project) {
        @NotNull final String query = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?);",
                getTableName(),
                IDBConstant.ROW_ID,
                IDBConstant.PROJECT_CREATED,
                IDBConstant.PROJECT_NAME,
                IDBConstant.PROJECT_DESCRIPTION,
                IDBConstant.PROJECT_STATUS,
                IDBConstant.USER_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, project.getId());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, Status.NOT_STARTED.name());
            statement.setString(6, project.getUserId());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project add(@Nullable final String userId, @Nullable final Project project) {
        project.setUserId(userId);
        return add(project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project update(@NotNull final Project project) {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                getTableName(),
                IDBConstant.PROJECT_NAME, IDBConstant.PROJECT_DESCRIPTION,
                IDBConstant.PROJECT_STATUS, IDBConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
