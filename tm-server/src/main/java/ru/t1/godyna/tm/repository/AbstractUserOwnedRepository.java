package ru.t1.godyna.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.constant.IDBConstant;
import ru.t1.godyna.tm.api.repository.IUserOwnedRepository;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public abstract M add(@Nullable final String userId, @Nullable final M model);

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        @NotNull final String query = String.format("DELETE FROM %s WHERE %s = ?;", getTableName(), IDBConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format("SELECT * FROM %s WHERE %s = ?;", getTableName(), IDBConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                while (rowSet.next()) result.add(fetch(rowSet));
                return result;
            }
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? ORDER_BY %s;", getTableName(), IDBConstant.USER_ID, getSortType(comparator));
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                while (rowSet.next()) result.add(fetch(rowSet));
                return result;
            }
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ? LIMIT 1;", getTableName(), IDBConstant.ROW_ID, IDBConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                if (!rowSet.next()) return null;
                return fetch(rowSet);
            }
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? AND %s = ?;", getTableName(), "ROW_NUMBER", IDBConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, index);
            statement.setString(2, userId);
            try (@NotNull final ResultSet rowSet = statement.executeQuery()) {
                if (!rowSet.next()) return null;
                return fetch(rowSet);
            }
        }
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        @NotNull final String query = String.format(
                "DELETE FROM %s WHERE %s = ? AND %s = ?;", getTableName(), IDBConstant.USER_ID, IDBConstant.ROW_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, model.getUserId());
            statement.setString(2, model.getId());
            statement.executeUpdate();
            return model;
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        @NotNull final String query = String.format(
                "SELECT COUNT(1) FROM %s WHERE %s = ?;", getTableName(), IDBConstant.USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setString(1, userId);
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt("count");
            }
        }
    }

}
