package ru.t1.godyna.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.IRepository;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.IService;
import ru.t1.godyna.tm.exception.entity.EntityNotFoundException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.IndexIncorrectException;
import ru.t1.godyna.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository <M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(
            @NotNull final IConnectionService connectionService
    ) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @Nullable final M entity;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull final Collection<M> models) {
        @Nullable final Collection<M> entities;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entities = repository.add(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entities;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> models) {
        @Nullable final Collection<M> entities;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entities = repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entities;
    }

    @NotNull
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @Nullable final M entity;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.removeOne(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            @Nullable final M entity = repository.findOneById(id);
            if (entity == null) throw new EntityNotFoundException();
            return entity;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            @Nullable final M entity = repository.findOneByIndex(index);
            if (entity == null) throw new EntityNotFoundException();
            return entity;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M entity;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.removeOneById(id);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable final M entity;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.removeOneByIndex(index);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

    @Override
    @SneakyThrows
    public long getSize() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable Collection<M> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll(collection);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.removeAll();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M update(@NotNull final M model) {
        @NotNull final Connection connection = getConnection();
        @Nullable final M entity;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            entity = repository.update(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return entity;
    }

}
