package ru.t1.godyna.tm.repository;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.constant.IDBConstant;
import ru.t1.godyna.tm.api.repository.IUserRepository;
import ru.t1.godyna.tm.enumerated.Role;
import ru.t1.godyna.tm.model.Task;
import ru.t1.godyna.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    @NotNull
    private final String tableName = IDBConstant.USER_TABLE;

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);",
                getTableName(),
                IDBConstant.ROW_ID, IDBConstant.LOGIN,
                IDBConstant.PASSWORD_HASH, IDBConstant.FST_NAME,
                IDBConstant.LST_NAME, IDBConstant.MDL_NAME,
                IDBConstant.EMAIL, IDBConstant.ROLE,
                IDBConstant.LOCK_FLG
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getFirstName());
            statement.setString(5, user.getLastName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getEmail());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.isLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1;",
                getTableName(),
                IDBConstant.LOGIN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String query = String.format(
                "SELECT * FROM %s WHERE %s = ? LIMIT 1;",
                getTableName(),
                IDBConstant.EMAIL
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            if (!rowSet.next()) return null;
            return fetch(rowSet);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@NotNull final String login) {
        @NotNull final String query = String.format(
                "SELECT COUNT(1) FROM %s WHERE %s = ?;",
                getTableName(),
                IDBConstant.LOGIN
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, login);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt("count") > 0;
            }
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isMailExist(@NotNull final String email) {
        @NotNull final String query = String.format(
                "SELECT COUNT(1) FROM %s WHERE %s = ?;",
                getTableName(),
                IDBConstant.EMAIL
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, email);
            @NotNull final ResultSet rowSet = statement.executeQuery();
            try (@NotNull final ResultSet resultSet = statement.executeQuery()) {
                resultSet.next();
                return resultSet.getInt("count") > 0;
            }
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User fetch(@NotNull final ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(IDBConstant.ROW_ID));
        user.setLogin(row.getString(IDBConstant.LOGIN));
        user.setPasswordHash(row.getString(IDBConstant.PASSWORD_HASH));
        user.setFirstName(row.getString(IDBConstant.FST_NAME));
        user.setLastName(row.getString(IDBConstant.LST_NAME));
        user.setMiddleName(row.getString(IDBConstant.MDL_NAME));
        user.setEmail(row.getString(IDBConstant.EMAIL));
        user.setRole(Role.toRole(row.getString(IDBConstant.ROLE)));
        user.setLocked(row.getBoolean(IDBConstant.LOCK_FLG));
        return user;
    }

    @Override
    @SneakyThrows
    public User update(@NotNull final User user) {
        @NotNull final String query = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                getTableName(),
                IDBConstant.LOGIN, IDBConstant.PASSWORD_HASH,
                IDBConstant.FST_NAME, IDBConstant.LST_NAME,
                IDBConstant.MDL_NAME, IDBConstant.EMAIL,
                IDBConstant.ROLE, IDBConstant.LOCK_FLG,
                IDBConstant.ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getFirstName());
            statement.setString(4, user.getLastName());
            statement.setString(5, user.getMiddleName());
            statement.setString(6, user.getEmail());
            statement.setString(7, user.getRole().toString());
            statement.setBoolean(8, user.isLocked());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

}
