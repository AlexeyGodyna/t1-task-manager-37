package ru.t1.godyna.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.godyna.tm.api.repository.ISessionRepository;
import ru.t1.godyna.tm.api.repository.IUserOwnedRepository;
import ru.t1.godyna.tm.api.service.IConnectionService;
import ru.t1.godyna.tm.api.service.ISessionService;
import ru.t1.godyna.tm.model.Session;
import ru.t1.godyna.tm.repository.SessionRepository;

import java.sql.Connection;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(@NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
