package ru.t1.godyna.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.model.Project;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final Project project) {
        super(project);
    }

}
